#!/usr/bin/env python
# Comparaison des mesures de dissemblance selon différentes cardinalité des ensembles 
import numpy as np
from scipy.stats import binom, hypergeom, chi2_contingency
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

def CalculPval(measure, Q, T, C):
	G = 100
	if measure == 'binomial' : 
		pval = binom.cdf( Q - C, Q, 1 - T/G)
	elif measure == 'hypergeometric' :
		pval = hypergeom.sf(C - 1, G, T, Q)
	elif measure == 'chi2' :
		contingence = np.array([[C, (Q - C)],
								[(T - C), (G - Q - T) + C]])
		pval = chi2_contingency(contingence)[1]
	elif measure == 'coverage' : 
		pval = 1 - (C / Q) * (C / T)
	return pval

# Fonction pour l'automatisation de création de matrice avec les différentes valeurs de cardinalités des ensembles 
def CreationVecs(measure) : 
	vecQ = []
	vecT = []
	vecC = []
	vecpval = []
	for Q in range(1, 30) :
		for T in range (1, 30) :
			for C in range (1, min(Q, T)) :
				vecQ.append(Q)
				vecT.append(T)
				vecC.append(C)
				vecpval.append(CalculPval(measure, Q, T, C))
	return (vecQ, vecT, vecC, vecpval)

Q_binomial, T_binomial, C_binomial, pval_binomial = CreationVecs('binomial')
Q_hypergeometric, T_hypergeometric, C_hypergeometric, pval_hypergeometric = CreationVecs('hypergeometric')
Q_chi2, T_chi2, C_chi2, pval_chi2 = CreationVecs('chi2')
Q_coverage, T_coverage, C_coverage, pval_coverage = CreationVecs('coverage')


def Visualisation(vecQ, vecT, vecC, vecpval):
	fig = plt.figure()
	ax = fig.gca(projection = '3d')
	surface = ax.scatter(vecQ, vecT, vecC, zdir = 'z', s = 30, c = vecpval, depthshade = True)
	fig.colorbar(surface)
	title = ax.set_title("pvalue en fonction de différentes tailles de Q, T and C")
	title.set_y(1.01)
	plt.show()

Visualisation(Q_binomial, T_binomial, C_binomial, pval_binomial)
Visualisation(Q_hypergeometric, T_hypergeometric, C_hypergeometric, pval_hypergeometric)
Visualisation(Q_chi2, T_chi2, C_chi2, pval_chi2)
Visualisation(Q_coverage, T_coverage, C_coverage, pval_coverage)

